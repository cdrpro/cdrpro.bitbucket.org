/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	/**
	 *
	 * new App()
	 * => AppSettings / AppState { ActiveTool, ActiveShape... }
	 * => core (work with app elements)
	 * => App Events { onSelectionChange, onToolChange... }
	 *
	 * Classes { AppDocument, SvgDocument?, SvgShape, AppTool, SvgTransform... }
	 *
	 */
	"use strict";
	__webpack_require__(1);
	const SvgDRAWApplication_1 = __webpack_require__(2);
	const SvgDRAWDocument_1 = __webpack_require__(3);
	class SvgDRAW {
	    constructor() {
	        const svgDoc = document.getElementById('SvgDRAW');
	        SvgDRAWApplication_1.default.activeDocument = new SvgDRAWDocument_1.default(svgDoc);
	        const zoomInput = document.getElementById('zoom-input');
	        zoomInput.addEventListener('change', event => this.onZoomChange(event), true);
	        window.addEventListener('resize', () => this.onResize(), false);
	    }
	    onZoomChange(event) {
	        const input = event.target;
	        SvgDRAWApplication_1.default.activeDocument.onZoomChange(+input.value);
	    }
	    onResize() {
	        clearTimeout(this.resizeTimer);
	        this.resizeTimer = setTimeout(() => {
	            SvgDRAWApplication_1.default.activeDocument.onWindowResize();
	        }, 10);
	    }
	}
	new SvgDRAW();


/***/ },
/* 1 */
/***/ function(module, exports) {

	// removed by extract-text-webpack-plugin

/***/ },
/* 2 */
/***/ function(module, exports) {

	"use strict";
	const SvgDRAWApplication = {
	    activeDocument: null,
	    activeShape: null,
	    isChrome: !!window['chrome']
	};
	Object.defineProperty(exports, "__esModule", { value: true });
	exports.default = SvgDRAWApplication;


/***/ },
/* 3 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	const SvgDRAWApplication_1 = __webpack_require__(2);
	const SvgShape_1 = __webpack_require__(4);
	const SvgDRAWDocControls_1 = __webpack_require__(8);
	const helpers_1 = __webpack_require__(5);
	class SvgDRAWDocument {
	    constructor(domElement) {
	        this.isMouseDown = false;
	        this.isFirstMove = false;
	        this.startMousePoint = null;
	        this.startShapePoint = null;
	        this.domElement = domElement;
	        this.svgDRAWDocBG = domElement.querySelector('#SvgDRAWDocBG');
	        this.svgDRAWDoc = domElement.querySelector('#SvgDRAWDoc');
	        this.docControls = new SvgDRAWDocControls_1.default(domElement.querySelector('#SvgDRAWDocControls'));
	        // todo:
	        this.width = 200;
	        this.height = 200;
	        this.zoom = 100;
	        this.updateWindowSize();
	        this.updateScreenSize();
	        this.moveToCenter();
	        this.bindEventHandlers();
	    }
	    // onSelectionChange(): void {
	    //
	    // }
	    bindEventHandlers() {
	        this.domElement.addEventListener('click', event => this.onDocumentClick(event), true);
	        this.domElement.addEventListener('mousedown', event => this.onMouseDown(event), true);
	        this.domElement.addEventListener('mousemove', event => this.onMouseMove(event), true);
	        this.domElement.addEventListener('mouseup', event => this.onMouseUp(event), true);
	    }
	    updateScreenSize() {
	        this.screenWidth = helpers_1.getZoomedValue(this.width, this.zoom);
	        this.screenHeight = helpers_1.getZoomedValue(this.height, this.zoom);
	    }
	    updateWindowSize() {
	        this.windowWidth = this.domElement.clientWidth;
	        this.windowHeight = this.domElement.clientHeight;
	    }
	    moveToCenter() {
	        const X = (this.windowWidth - this.screenWidth) / 2;
	        const Y = (this.windowHeight - this.screenHeight) / 2;
	        helpers_1.setDomAttributes(this.svgDRAWDocBG, { x: X, y: Y });
	        helpers_1.setDomAttributes(this.svgDRAWDoc, { x: X, y: Y });
	        this.docControls.moveTo(X, Y);
	    }
	    onWindowResize() {
	        this.updateWindowSize();
	        this.moveToCenter();
	    }
	    onZoomChange(zoom) {
	        this.zoom = zoom;
	        this.updateScreenSize();
	        helpers_1.setDomAttributes(this.svgDRAWDocBG, {
	            width: this.screenWidth,
	            height: this.screenHeight
	        });
	        helpers_1.setDomAttributes(this.svgDRAWDoc, {
	            width: this.screenWidth,
	            height: this.screenHeight
	        });
	        // todo: remove in the future?
	        this.moveToCenter();
	        const selector = this.docControls;
	        selector.feetToSelection(this.zoom);
	    }
	    onDocumentClick(event) {
	        const target = event.target;
	        const selector = this.docControls;
	        if (target && target !== this.domElement && !target.classList.contains('select-mrk')) {
	            const aShape = SvgDRAWApplication_1.default.activeShape;
	            // if same shape, just return
	            if (aShape && aShape.domElement === target)
	                return;
	            event.preventDefault();
	            event.stopPropagation();
	            SvgDRAWApplication_1.default.activeShape = new SvgShape_1.default(target);
	            selector.feetToSelection(this.zoom);
	            selector.show();
	        }
	        else {
	            SvgDRAWApplication_1.default.activeShape = null;
	            selector.hide();
	        }
	    }
	    /**
	     *
	     * @param event
	     */
	    onMouseDown(event) {
	        this.isMouseDown = true;
	        this.isFirstMove = true;
	        this.startMousePoint = {
	            x: event.layerX,
	            y: event.layerY
	        };
	        // console.log('start');
	    }
	    /**
	     *
	     * @param event
	     */
	    onMouseMove(event) {
	        if (this.isMouseDown !== true)
	            return;
	        const target = event.target;
	        const selector = this.docControls;
	        if (this.isFirstMove) {
	            if (SvgDRAWApplication_1.default.activeShape === null || SvgDRAWApplication_1.default.activeShape.domElement !== target) {
	                SvgDRAWApplication_1.default.activeShape = new SvgShape_1.default(target);
	                selector.show();
	            }
	            this.isFirstMove = false;
	        }
	        const aShape = SvgDRAWApplication_1.default.activeShape;
	        if (this.startShapePoint === null) {
	            this.startShapePoint = aShape.getPosition();
	        }
	        let diffX = event.layerX - this.startMousePoint.x;
	        let diffY = event.layerY - this.startMousePoint.y;
	        if (this.zoom !== 100) {
	            const shift = this.zoom / 100;
	            diffX = +(diffX / shift).toFixed(3);
	            diffY = +(diffY / shift).toFixed(3);
	        }
	        selector.feetToSelection(this.zoom);
	        aShape.setPosition(this.startShapePoint.x + diffX, this.startShapePoint.y + diffY);
	    }
	    /**
	     *
	     * @param event
	     */
	    onMouseUp(event) {
	        this.isMouseDown = false;
	        this.isFirstMove = false;
	        this.startMousePoint = null;
	        this.startShapePoint = null;
	        // console.log('end');
	    }
	}
	Object.defineProperty(exports, "__esModule", { value: true });
	exports.default = SvgDRAWDocument;


/***/ },
/* 4 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	const SvgDRAWApplication_1 = __webpack_require__(2);
	const helpers_1 = __webpack_require__(5);
	const TYPE_CIRCLE = 'circle';
	const TYPE_RECT = 'rect';
	class SvgShape {
	    constructor(el) {
	        this.domElement = el;
	    }
	    getPosition() {
	        switch (this.domElement.nodeName) {
	            case TYPE_CIRCLE:
	                return {
	                    x: this.domElement.cx.baseVal.value,
	                    y: this.domElement.cy.baseVal.value
	                };
	            case TYPE_RECT:
	                return {
	                    x: this.domElement.x.baseVal.value,
	                    y: this.domElement.y.baseVal.value
	                };
	        }
	        return undefined;
	    }
	    setPosition(x, y) {
	        switch (this.domElement.nodeName) {
	            case TYPE_CIRCLE: helpers_1.setDomAttributes(this.domElement, { cx: x, cy: y });
	            case TYPE_RECT: helpers_1.setDomAttributes(this.domElement, { x: x, y: y });
	        }
	        // hack for chrome/opera
	        if (SvgDRAWApplication_1.default.isChrome) {
	            const display = this.domElement.style.display;
	            this.domElement.style.display = 'block';
	            setTimeout(() => this.domElement.style.display = display, 0);
	        }
	    }
	    getBoundingBox() {
	        return this.domElement.getBBox();
	    }
	    getTransform() {
	        const transform = this.domElement.getAttribute('transform');
	        return transform ? helpers_1.parseTransformString(transform) : null;
	    }
	    setTransform(obj) {
	        // this._el.style.transform = core.transformToStyle(obj);
	        // this._el.removeAttribute('transform');
	        this.domElement.setAttribute('transform', helpers_1.transformToString(obj));
	    }
	    removeTransform() {
	        this.domElement.removeAttribute('transform');
	        this.domElement.style.transform = null;
	    }
	}
	Object.defineProperty(exports, "__esModule", { value: true });
	exports.default = SvgShape;


/***/ },
/* 5 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	var math_1 = __webpack_require__(6);
	exports.getZoomedValue = math_1.getZoomedValue;
	var htmlElement_1 = __webpack_require__(7);
	exports.parseTransformString = htmlElement_1.parseTransformString;
	exports.transformToString = htmlElement_1.transformToString;
	exports.setDomAttributes = htmlElement_1.setDomAttributes;


/***/ },
/* 6 */
/***/ function(module, exports) {

	"use strict";
	function getZoomedValue(val, zoom) {
	    return val * zoom / 100;
	}
	exports.getZoomedValue = getZoomedValue;


/***/ },
/* 7 */
/***/ function(module, exports) {

	"use strict";
	function parseTransformString(str) {
	    const transforms = {};
	    const match = str.match(/(\w+\((\-?\d+\.?\d*e?\-?\d*,?)+\))+/g);
	    for (const i in match) {
	        const c = match[i].match(/[\w\.\-]+/g);
	        transforms[c.shift()] = c;
	    }
	    return transforms;
	}
	exports.parseTransformString = parseTransformString;
	function transformToString(t) {
	    const props = [];
	    for (const name in t) {
	        if (!t.hasOwnProperty(name))
	            continue;
	        const prop = name + '(' + t[name].join(',') + ')';
	        props.push(prop);
	    }
	    return props.join(' ');
	}
	exports.transformToString = transformToString;
	function setDomAttributes(el, attributes) {
	    for (var key in attributes) {
	        if (!attributes.hasOwnProperty(key))
	            continue;
	        el.setAttribute(key, attributes[key]);
	    }
	}
	exports.setDomAttributes = setDomAttributes;


/***/ },
/* 8 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	const SvgDRAWApplication_1 = __webpack_require__(2);
	const SvgDRAWSelectionFrame_1 = __webpack_require__(9);
	const helpers_1 = __webpack_require__(5);
	class SvgDRAWDocControls {
	    constructor(el) {
	        this.domElement = el;
	        this.selectionFrame = new SvgDRAWSelectionFrame_1.default(el.querySelector('#SvgDRAWSelectionFrame'));
	    }
	    // todo:
	    feetToSelection(zoom) {
	        const shape = SvgDRAWApplication_1.default.activeShape;
	        if (shape === null)
	            return;
	        const bb = shape.getBoundingBox();
	        const transform = shape.getTransform();
	        const points = {
	            left: helpers_1.getZoomedValue(bb.x, zoom),
	            top: helpers_1.getZoomedValue(bb.y, zoom),
	            right: helpers_1.getZoomedValue(bb.x, zoom) + helpers_1.getZoomedValue(bb.width, zoom),
	            bottom: helpers_1.getZoomedValue(bb.y, zoom) + helpers_1.getZoomedValue(bb.height, zoom)
	        };
	        for (const marker of this.selectionFrame.markers) {
	            marker.setPosition(points[marker.xSide], points[marker.ySide]);
	        }
	        if (transform) {
	            for (const t in transform) {
	                if (!transform.hasOwnProperty(t))
	                    continue;
	                if (t === 'rotate' && transform[t].length === 3) {
	                    transform[t][1] = helpers_1.getZoomedValue(transform[t][1], zoom);
	                    transform[t][2] = helpers_1.getZoomedValue(transform[t][2], zoom);
	                }
	            }
	            this.selectionFrame.setTransform(transform);
	        }
	        else {
	            this.selectionFrame.removeTransform();
	        }
	    }
	    moveTo(x, y) {
	        this.domElement.setAttribute('transform', `translate(${x},${y})`);
	    }
	    show() {
	        this.selectionFrame.show();
	    }
	    hide() {
	        this.selectionFrame.hide();
	    }
	}
	Object.defineProperty(exports, "__esModule", { value: true });
	exports.default = SvgDRAWDocControls;


/***/ },
/* 9 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	const SvgShape_1 = __webpack_require__(4);
	const SvgDRAWSelectionMarker_1 = __webpack_require__(10);
	const helpers_1 = __webpack_require__(5);
	const SVGNS = "http://www.w3.org/2000/svg";
	const SIZE_PX = 8;
	const MARKERS = [
	    { name: 'tl', x: 'left', y: 'top' },
	    { name: 'tr', x: 'right', y: 'top' },
	    { name: 'br', x: 'right', y: 'bottom' },
	    { name: 'bl', x: 'left', y: 'bottom' },
	];
	const SIDES_MASK = {
	    X: { left: -SIZE_PX, right: 0 },
	    Y: { top: -SIZE_PX, bottom: 0 }
	};
	class SvgDRAWSelectionFrame extends SvgShape_1.default {
	    constructor(el) {
	        super(el);
	        this.createMarkers();
	    }
	    createMarkers() {
	        const rectTemplate = document.createElementNS(SVGNS, 'rect');
	        helpers_1.setDomAttributes(rectTemplate, { width: SIZE_PX, height: SIZE_PX, fill: '#4f80ff' });
	        this.markers = [];
	        for (const marker of MARKERS) {
	            const rect = rectTemplate.cloneNode();
	            rect.setAttribute('transform', `translate(${SIDES_MASK.X[marker.x]},${SIDES_MASK.Y[marker.y]})`);
	            this.domElement.appendChild(rect);
	            this.markers.push(new SvgDRAWSelectionMarker_1.default(rect, marker));
	        }
	    }
	    show() {
	        this.domElement.setAttribute('display', '');
	    }
	    hide() {
	        this.domElement.setAttribute('display', 'none');
	    }
	}
	Object.defineProperty(exports, "__esModule", { value: true });
	exports.default = SvgDRAWSelectionFrame;


/***/ },
/* 10 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	const SvgShape_1 = __webpack_require__(4);
	class SvgDRAWSelectionMarker extends SvgShape_1.default {
	    constructor(el, props) {
	        super(el);
	        this.name = props.name;
	        this.xSide = props.x;
	        this.ySide = props.y;
	    }
	}
	Object.defineProperty(exports, "__esModule", { value: true });
	exports.default = SvgDRAWSelectionMarker;


/***/ }
/******/ ]);
//# sourceMappingURL=bundle.js.map